$(function() {
    // スマホ最大幅
    const smWidth = 768;
    // 現在の幅
    var $windowWidth = $(window).width();

    // アコーディオン
    $('.question_list .question').click(function(){
        if($(this).hasClass('active')) {
            $(this).removeClass('active');
        } 
        else {
            $(this).addClass('active');
        }
        $(this).next('.answer').slideToggle();
    });

    // ハンバーガーメニュー
    $('.menu_ham').click(function(){
        if($('.ham_line').hasClass('active')) {
            $('.ham_line').removeClass('active');
            // $('.menu').slideUp(function(){
            //     //$(this).parent().hide();
            // });
            $('.menu').slideUp();
        }
        else {
            $('.ham_line').addClass('active');
            //$('.menu').hide().parent().show().end().slideDown();
            $('.menu').slideDown();
        }
    });
    // スマホサイズ時のメニューのページ内リンクを押した時の処理
    $('.menu a').click(function(){
        var $windowWidth = $(window).width();
        if(!($windowWidth > smWidth)) {
            $('.ham_line').removeClass('active');
            $('.menu').slideUp();
        }
    });
    // リサイズ時のdisplay周りの処理
    $(window).resize(function(){
        var $windowWidth = $(window).width();
        if($windowWidth > smWidth) {
            $('.menu').css('display', 'block');
            $('.ham_line').removeClass('active');
        }
        else if (!$('.ham_line').hasClass('active')){
            $('.menu').css('display', 'none');
        }
    });
});